<?php

namespace App;

use App\Registry;

class App
{
    public static $app;

    public function __construct()
    {
        $query              = trim( $_SERVER['REQUEST_URI'] );
        self::$app          = Registry::instance();
        $this->getParams();
        new ErrorHandler();

        Router::dispatch( $query );
    }

    protected function getParams()
    {
        $params = require_once CONF . '/config.php';
        if(!empty($params)){
            foreach($params as $k => $v){
                self::$app->setProperty($k, $v);
            }
        }
    }
}