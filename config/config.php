<?php

return [
    'db_name' => '',
    'db_user' => '',
    'db_password' => '',
    'db_host' => 'localhost',
    'db_charset' => 'utf8',
];