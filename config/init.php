<?php

define( "DEBUG", 1 );
define( "ROOT", dirname( __DIR__ ) );
define( "APP", ROOT . '/app' );
define( "WWW", ROOT . '/public' );
define( "CONF", ROOT . '/config' );
define( "LAYOUT", 'default' );

require_once ROOT . '/vendor/autoload.php';
