<?php

namespace App\controllers;

use App\View;

abstract class Controller
{
    public $route;
    public $controller;
    public $model;
    public $view;
    public $layout;
    public $data                    = [];
    public $meta                    = [ 'title' => '', 'desc' => '', 'keywords' => '' ];

    public function __controller( $route )
    {
        $this->view                 = $route[ 'action' ];
        $this->model                = $route[ 'controller' ];
        $this->route                = $route;
        $this->controller           = $route[ 'controller' ];
    }

    public function getView()
    {
        $viewObject                 = new View( $this->route, $this->layout, $this->view, $this->meta );
        $viewObject->render($this->data);
    }

    public function setData($data)
    {
        $this->data                 = $data;
    }

    public function setMeta( $title = '', $desc = '', $keywords = '' )
    {
        $this->meta['title']        = $title;
        $this->meta['desc']         = $desc;
        $this->meta['keywords']     = $keywords;
    }
}