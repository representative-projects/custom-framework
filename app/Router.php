<?php

namespace App;

class Router
{
    protected static $routes            = [];

    protected static $route             = [];

    public static function add( $route, $action = [] )
    {
       self::$routes[ $route ]          = $action;
    }

    public static function getRoutes()
    {
        return self::$routes;
    }

    public static function getRoute()
    {
        return self::$route;
    }

    public static function dispatch( $url )
    {
        if ( self::matchRoute( $url ) ) {
            $controller                 = 'App\controllers\\' . self::$route['controller'] . 'Controller';
            if ( class_exists( $controller ) ) {
                $controllerObject       = new $controller( self::$route );
                $action                 = self::lowerCamelCase( self::$route['action'] ) . 'Action';
                if(method_exists( $controllerObject, $action )){
                    $controllerObject->$action();
                }else{
                    throw new \Exception("Method $controller::$action not found", 404);
                }
            }else{
                throw new \Exception("Controller $controller not found", 404);
            }
        }else{
            throw new \Exception("Page not found", 404);
        }
    }

    public static function matchRoute( $url )
    {
        foreach( self::$routes as $pattern => $route ) {
            if( $pattern === $url ) {
                foreach( $route as $k => $v ) {
                    if( is_string( $k ) ) {
                        $route[$k]      = $v;
                    }
                }
                $route['controller']    = self::upperCamelCase($route['controller']);
                self::$route            = $route;
                return true;
            }
        }
        return false;
    }

    protected static function upperCamelCase($name){
        return str_replace(' ', '', ucwords(str_replace('-', ' ', $name)));
    }

    protected static function lowerCamelCase($name){
        return lcfirst(self::upperCamelCase($name));
    }
}